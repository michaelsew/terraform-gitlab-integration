terraform {
  backend "remote" {
    hostname = "app.terraform.io"
    organization = "ualano"

    workspaces {
      prefix = "tf-medium-"
    }
  }
}