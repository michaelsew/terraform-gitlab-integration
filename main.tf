# ##################################################
# TERRAFORM CONFIGURATION FOR DEV AND PROD PROJECTS
# ROOT MODULE
# ##################################################

resource "random_string" "project_suffix" {
  length  = 4
  upper   = false
  special = false
}

locals {
  env_name = lower(terraform.workspace) == "default" ? var.workspace : lower(terraform.workspace)
  suffix   = random_string.project_suffix.id
}

# #########################################
# PROJECT MODULE
# #########################################

module "project" {
  source          = "./modules/project"
  project_name    = "${var.project_name}-${local.env_name}"
  project_id      = "${var.project_id}-${local.env_name}-${(local.suffix)}"
  org_id          = var.org_id
  folder_id       = var.folder_id
  billing_account = var.billing_account
}

# #########################################
# CLOUD STORAGE MODULE
# #########################################

module "cloud-storage" {
  source        = "./modules/cloud-storage"
  project_id    = module.project.project_id
  region        = var.region
  suffix        = "${local.env_name}-${local.suffix}"
  buckets_map   = var.buckets
  common_labels = var.common_labels
}
