# #########################################
# VARIABLES VALUES
# #########################################

org_id          = ""
folder_id       = ""
project_id      = "tf-medium"
project_name    = "tf-medium"
creds           = "./gcp-sa.json"
region          = "us-central1"
billing_account = ""
buckets = {
  landing = "landing-zone"
  raw     = "raw-zone"
  curated = "curated-zone"
}