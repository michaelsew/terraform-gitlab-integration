# Terraform Test - Multi Environments using Workspaces and Terraform Cloud

1. Create Configuration that includes, as resources, a GCP project and few buckets
2. Create 2 Local Workspaces, 1 for DEV and 1 for PROD environment
3. Create a GitLab repository
4. Deploy Resources to GCP
5. Migrate TF state to Terraform Cloud
6. Link workspaces to the GitHub repo


When Migrating to Terraform Cloud, you need to create a GOOGLE_CREDENTIALS variable on Terraform Cloud and assign the content of the Service Account File to it. The content of the JSON file has to be modified before though to remove all the newline characters from the file. Follow below

Open the file with vi, then
press :

copy paste the following:
%s;\n; ;g

Press enter

press : again

type wq to save and exit