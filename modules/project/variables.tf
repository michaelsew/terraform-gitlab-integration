# #########################################
# VARIABLES DEFINITION
# #########################################

variable "org_id" {
  type        = string
  default     = ""
  description = "Organization ID"
}

variable "folder_id" {
  type        = string
  default     = ""
  description = "Folder ID"
}

variable "billing_account" {
  type        = string
  default     = ""
  description = "Billing Account"
}

variable "project_id" {
  type        = string
  default     = ""
  description = "Project ID"
}

variable "project_name" {
  type        = string
  default     = ""
  description = "Project Name"
}

