# #########################################
# OUTPUTS
# #########################################

output "project_id" {
  value = replace(google_project.project.id, "projects/", "")
}

output "project_name" {
  value = google_project.project.name
}