# #########################################
# LOCALS
# #########################################

locals {
  age = {
    landing = 14
    raw     = 90
    curated = 365
    backend = 365
  }

  action = {
    landing = "Delete"
    raw     = "SetStorageClass"
    curated = "SetStorageClass"
    backend = "SetStorageClass"
  }

  storage_class = {
    raw     = "NEARLINE"
    curated = "COLDLINE"
    backend = "NEARLINE"
  }
}


# #########################################
# BUCKETS
# #########################################

# Create Buckets using a for each loop.
resource "google_storage_bucket" "project-buckets" {
  for_each = var.buckets_map

  project       = var.project_id
  name          = "${each.value}-${var.suffix}"
  location      = var.region
  force_destroy = true

  labels = merge(var.common_labels)

  lifecycle_rule {
    condition {
      age = lookup(local.age, "${each.key}", 0)
    }
    action {
      type          = lookup(local.action, "${each.key}", "Delete")
      storage_class = lookup(local.storage_class, "${each.key}", "")
    }
  }

}
